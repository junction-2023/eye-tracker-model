# import libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# read driving p1 data and store in dataframe

print("read driving p1 data and store in dataframe")
driving_p1_afe_000 = pd.read_json('../../driving/p1/AFE_000_CONFIDENTIAL.json')
driving_p1_afe_001 = pd.read_json('../../driving/p1/AFE_001_CONFIDENTIAL.json')
driving_p1_afe_002 = pd.read_json('../../driving/p1/AFE_002_CONFIDENTIAL.json')
driving_p1_afe_003 = pd.read_json('../../driving/p1/AFE_003_CONFIDENTIAL.json')

driving_p1_imu_000 = pd.read_json('../../driving/p1/IMU_000_CONFIDENTIAL.json')
driving_p1_imu_001 = pd.read_json('../../driving/p1/IMU_001_CONFIDENTIAL.json')
driving_p1_imu_002 = pd.read_json('../../driving/p1/IMU_002_CONFIDENTIAL.json')
driving_p1_imu_003 = pd.read_json('../../driving/p1/IMU_003_CONFIDENTIAL.json')

# read driving p2 data and store in dataframe

print("read driving p2 data and store in dataframe")
driving_p2_afe_000 = pd.read_json('../../driving/p2/AFE_000_CONFIDENTIAL.json')
driving_p2_afe_001 = pd.read_json('../../driving/p2/AFE_001_CONFIDENTIAL.json')
driving_p2_afe_002 = pd.read_json('../../driving/p2/AFE_002_CONFIDENTIAL.json')
driving_p2_afe_003 = pd.read_json('../../driving/p2/AFE_003_CONFIDENTIAL.json')

driving_p2_imu_000 = pd.read_json('../../driving/p2/IMU_000_CONFIDENTIAL.json')
driving_p2_imu_001 = pd.read_json('../../driving/p2/IMU_001_CONFIDENTIAL.json')
driving_p2_imu_002 = pd.read_json('../../driving/p2/IMU_002_CONFIDENTIAL.json')
driving_p2_imu_003 = pd.read_json('../../driving/p2/IMU_003_CONFIDENTIAL.json')

# read driving p3 data and store in dataframe

print("read driving p3 data and store in dataframe")
driving_p3_afe_000 = pd.read_json('../../driving/p3/AFE_000_CONFIDENTIAL.json')
driving_p3_afe_001 = pd.read_json('../../driving/p3/AFE_001_CONFIDENTIAL.json')
driving_p3_afe_002 = pd.read_json('../../driving/p3/AFE_002_CONFIDENTIAL.json')
driving_p3_afe_003 = pd.read_json('../../driving/p3/AFE_003_CONFIDENTIAL.json')

driving_p3_imu_000 = pd.read_json('../../driving/p3/IMU_000_CONFIDENTIAL.json')
driving_p3_imu_001 = pd.read_json('../../driving/p3/IMU_001_CONFIDENTIAL.json')
driving_p3_imu_002 = pd.read_json('../../driving/p3/IMU_002_CONFIDENTIAL.json')
driving_p3_imu_003 = pd.read_json('../../driving/p3/IMU_003_CONFIDENTIAL.json')

# read indoor p1 data and store in dataframe

print("read indoor p1 data and store in dataframe")
indoor_p1_afe_000 = pd.read_json('../../indoor/p1/AFE_000_CONFIDENTIAL.json')
indoor_p1_afe_001 = pd.read_json('../../indoor/p1/AFE_001_CONFIDENTIAL.json')
indoor_p1_afe_002 = pd.read_json('../../indoor/p1/AFE_002_CONFIDENTIAL.json')
indoor_p1_afe_003 = pd.read_json('../../indoor/p1/AFE_003_CONFIDENTIAL.json')

indoor_p1_imu_000 = pd.read_json('../../indoor/p1/IMU_000_CONFIDENTIAL.json')
indoor_p1_imu_001 = pd.read_json('../../indoor/p1/IMU_001_CONFIDENTIAL.json')
indoor_p1_imu_002 = pd.read_json('../../indoor/p1/IMU_002_CONFIDENTIAL.json')
indoor_p1_imu_003 = pd.read_json('../../indoor/p1/IMU_003_CONFIDENTIAL.json')

# read indoor p2 data and store in dataframe

print("read indoor p2 data and store in dataframe")
indoor_p2_afe_000 = pd.read_json('../../indoor/p2/AFE_000_CONFIDENTIAL.json')
indoor_p2_afe_001 = pd.read_json('../../indoor/p2/AFE_001_CONFIDENTIAL.json')
indoor_p2_afe_002 = pd.read_json('../../indoor/p2/AFE_002_CONFIDENTIAL.json')
indoor_p2_afe_003 = pd.read_json('../../indoor/p2/AFE_003_CONFIDENTIAL.json')

indoor_p2_imu_000 = pd.read_json('../../indoor/p2/IMU_000_CONFIDENTIAL.json')
indoor_p2_imu_001 = pd.read_json('../../indoor/p2/IMU_001_CONFIDENTIAL.json')
indoor_p2_imu_002 = pd.read_json('../../indoor/p2/IMU_002_CONFIDENTIAL.json')
indoor_p2_imu_003 = pd.read_json('../../indoor/p2/IMU_003_CONFIDENTIAL.json')

# read indoor p3 data and store in dataframe

print("read indoor p3 data and store in dataframe")
indoor_p3_afe_000 = pd.read_json('../../indoor/p3/AFE_000_CONFIDENTIAL.json')
indoor_p3_afe_001 = pd.read_json('../../indoor/p3/AFE_001_CONFIDENTIAL.json')
indoor_p3_afe_002 = pd.read_json('../../indoor/p3/AFE_002_CONFIDENTIAL.json')
indoor_p3_afe_003 = pd.read_json('../../indoor/p3/AFE_003_CONFIDENTIAL.json')

indoor_p3_imu_000 = pd.read_json('../../indoor/p3/IMU_000_CONFIDENTIAL.json')
indoor_p3_imu_001 = pd.read_json('../../indoor/p3/IMU_001_CONFIDENTIAL.json')
indoor_p3_imu_002 = pd.read_json('../../indoor/p3/IMU_002_CONFIDENTIAL.json')
indoor_p3_imu_003 = pd.read_json('../../indoor/p3/IMU_003_CONFIDENTIAL.json')

# read walking p1 data and store in dataframe

print("read walking p1 data and store in dataframe")
walking_p1_afe_000 = pd.read_json('../../walking/p1/AFE_000_CONFIDENTIAL.json')
walking_p1_afe_001 = pd.read_json('../../walking/p1/AFE_001_CONFIDENTIAL.json')

walking_p1_imu_000 = pd.read_json('../../walking/p1/IMU_000_CONFIDENTIAL.json')
walking_p1_imu_001 = pd.read_json('../../walking/p1/IMU_001_CONFIDENTIAL.json')

# read walking p2 data and store in dataframe

print("read walking p2 data and store in dataframe")
walking_p2_afe_000 = pd.read_json('../../walking/p2/AFE_000_CONFIDENTIAL.json')
walking_p2_afe_001 = pd.read_json('../../walking/p2/AFE_001_CONFIDENTIAL.json')

walking_p2_imu_000 = pd.read_json('../../walking/p2/IMU_000_CONFIDENTIAL.json')
walking_p2_imu_001 = pd.read_json('../../walking/p2/IMU_001_CONFIDENTIAL.json')

# read walking p3 data and store in dataframe

print("read walking p3 data and store in dataframe")
walking_p3_afe_000 = pd.read_json('../../walking/p3/AFE_000_CONFIDENTIAL.json')
walking_p3_afe_001 = pd.read_json('../../walking/p3/AFE_001_CONFIDENTIAL.json')
walking_p3_afe_002 = pd.read_json('../../walking/p3/AFE_002_CONFIDENTIAL.json')

walking_p3_imu_000 = pd.read_json('../../walking/p3/IMU_000_CONFIDENTIAL.json')
walking_p3_imu_001 = pd.read_json('../../walking/p3/IMU_001_CONFIDENTIAL.json')
walking_p3_imu_002 = pd.read_json('../../walking/p3/IMU_002_CONFIDENTIAL.json')

# driving p1 data
driving_p1_afe = [driving_p1_afe_000, driving_p1_afe_001, driving_p1_afe_002, driving_p1_afe_003]
driving_p1_imu = [driving_p1_imu_000, driving_p1_imu_001, driving_p1_imu_002, driving_p1_imu_003]

# driving p2 data
driving_p2_afe = [driving_p2_afe_000, driving_p2_afe_001, driving_p2_afe_002, driving_p2_afe_003]
driving_p2_imu = [driving_p2_imu_000, driving_p2_imu_001, driving_p2_imu_002, driving_p2_imu_003]

# driving p3 data
driving_p3_afe = [driving_p3_afe_000, driving_p3_afe_001, driving_p3_afe_002, driving_p3_afe_003]
driving_p3_imu = [driving_p3_imu_000, driving_p3_imu_001, driving_p3_imu_002, driving_p3_imu_003]

# indoor p1 data
indoor_p1_afe = [indoor_p1_afe_000, indoor_p1_afe_001, indoor_p1_afe_002, indoor_p1_afe_003]
indoor_p1_imu = [indoor_p1_imu_000, indoor_p1_imu_001, indoor_p1_imu_002, indoor_p1_imu_003]

# indoor p2 data
indoor_p2_afe = [indoor_p2_afe_000, indoor_p2_afe_001, indoor_p2_afe_002, indoor_p2_afe_003]
indoor_p2_imu = [indoor_p2_imu_000, indoor_p2_imu_001, indoor_p2_imu_002, indoor_p2_imu_003]

# indoor p3 data
indoor_p3_afe = [indoor_p3_afe_000, indoor_p3_afe_001, indoor_p3_afe_002, indoor_p3_afe_003]
indoor_p3_imu = [indoor_p3_imu_000, indoor_p3_imu_001, indoor_p3_imu_002, indoor_p3_imu_003]

# walking p1 data
walking_p1_afe = [walking_p1_afe_000, walking_p1_afe_001]
walking_p1_imu = [walking_p1_imu_000, walking_p1_imu_001]

# walking p2 data
walking_p2_afe = [walking_p2_afe_000, walking_p2_afe_001]
walking_p2_imu = [walking_p2_imu_000, walking_p2_imu_001]

# walking p3 data
walking_p3_afe = [walking_p3_afe_000, walking_p3_afe_001, walking_p3_afe_002]
walking_p3_imu = [walking_p3_imu_000, walking_p3_imu_001, walking_p3_imu_002]

driving_afe = pd.concat(driving_p1_afe + driving_p2_afe + driving_p3_afe)
drving_aux = driving_afe['auxSensors']
driving_imu = pd.concat(driving_p1_imu + driving_p2_imu + driving_p3_imu)

indoor_afe = pd.concat(indoor_p1_afe + indoor_p2_afe + indoor_p3_afe)
indoor_aux = indoor_afe['auxSensors']
indoor_imu = pd.concat(indoor_p1_imu + indoor_p2_imu + indoor_p3_imu)

walking_afe = pd.concat(walking_p1_afe + walking_p2_afe + walking_p3_afe)
walking_aux = walking_afe['auxSensors']
walking_imu = pd.concat(walking_p1_imu + walking_p2_imu + walking_p3_imu)

driving_blinks = driving_afe['blinks'].reset_index(drop=True)
driving_blinks = driving_blinks[driving_blinks.notna()]
driving_blinks_index_l = []
driving_blinks_peak_l = []
driving_blinks_start_l = []
driving_blinks_end_l = []
driving_blinks_index_r = []
driving_blinks_peak_r = []
driving_blinks_start_r = []
driving_blinks_end_r = []
for i in driving_blinks:
    if ('left' in i):
        driving_blinks_index_l.append(i['left']['index'])
        driving_blinks_peak_l.append(i['left']['peakTime'])
        driving_blinks_start_l.append(i['left']['beginTime'])
        driving_blinks_end_l.append(i['left']['endTime'])
    if ('right' in i):
        driving_blinks_index_r.append(i['right']['index'])
        driving_blinks_peak_r.append(i['right']['peakTime'])
        driving_blinks_start_r.append(i['right']['beginTime'])
        driving_blinks_end_r.append(i['right']['endTime'])

driving_blinks_l = pd.DataFrame({'index': driving_blinks_index_l,
                                 'peakTime': driving_blinks_peak_l,
                                 'beginTime': driving_blinks_start_l,
                                 'endTime': driving_blinks_end_l})

driving_blinks_r = pd.DataFrame({'index': driving_blinks_index_r,
                                'peakTime': driving_blinks_peak_r,
                                'beginTime': driving_blinks_start_r,
                                'endTime': driving_blinks_end_r})

indoor_blinks = indoor_afe['blinks'].reset_index(drop=True)
indoor_blinks = indoor_blinks[indoor_blinks.notna()]
indoor_blinks_index_l = []
indoor_blinks_peak_l = []
indoor_blinks_start_l = []
indoor_blinks_end_l = []
indoor_blinks_index_r = []
indoor_blinks_peak_r = []
indoor_blinks_start_r = []
indoor_blinks_end_r = []
for i in indoor_blinks:
    if ('left' in i):
        indoor_blinks_index_l.append(i['left']['index'])
        indoor_blinks_peak_l.append(i['left']['peakTime'])
        indoor_blinks_start_l.append(i['left']['beginTime'])
        indoor_blinks_end_l.append(i['left']['endTime'])
    if ('right' in i):
        indoor_blinks_index_r.append(i['right']['index'])
        indoor_blinks_peak_r.append(i['right']['peakTime'])
        indoor_blinks_start_r.append(i['right']['beginTime'])
        indoor_blinks_end_r.append(i['right']['endTime'])

indoor_blinks_l = pd.DataFrame({'index': indoor_blinks_index_l,
                                 'peakTime': indoor_blinks_peak_l,
                                 'beginTime': indoor_blinks_start_l,
                                 'endTime': indoor_blinks_end_l})

indoor_blinks_r = pd.DataFrame({'index': indoor_blinks_index_r,
                                'peakTime': indoor_blinks_peak_r,
                                'beginTime': indoor_blinks_start_r,
                                'endTime': indoor_blinks_end_r})

walking_blinks = walking_afe['blinks'].reset_index(drop=True)
walking_blinks = walking_blinks[walking_blinks.notna()]
walking_blinks_index_l = []
walking_blinks_peak_l = []
walking_blinks_start_l = []
walking_blinks_end_l = []
walking_blinks_index_r = []
walking_blinks_peak_r = []
walking_blinks_start_r = []
walking_blinks_end_r = []
for i in walking_blinks:
    if ('left' in i):
        walking_blinks_index_l.append(i['left']['index'])
        walking_blinks_peak_l.append(i['left']['peakTime'])
        walking_blinks_start_l.append(i['left']['beginTime'])
        walking_blinks_end_l.append(i['left']['endTime'])
    if ('right' in i):
        walking_blinks_index_r.append(i['right']['index'])
        walking_blinks_peak_r.append(i['right']['peakTime'])
        walking_blinks_start_r.append(i['right']['beginTime'])
        walking_blinks_end_r.append(i['right']['endTime'])

walking_blinks_l = pd.DataFrame({'index': walking_blinks_index_l,
                                 'peakTime': walking_blinks_peak_l,
                                 'beginTime': walking_blinks_start_l,
                                 'endTime': walking_blinks_end_l})

walking_blinks_r = pd.DataFrame({'index': walking_blinks_index_r,
                                'peakTime': walking_blinks_peak_r,
                                'beginTime': walking_blinks_start_r,
                                'endTime': walking_blinks_end_r})

driving_afe.drop(['battery', 'blinks', 'labels', 'gps', 'auxSensors'], axis=1, inplace=True)
indoor_afe.drop(['battery', 'blinks', 'labels', 'auxSensors'], axis=1, inplace=True)
walking_afe.drop(['battery', 'blinks', 'auxSensors'], axis=1, inplace=True)

# Left eye data
i_0_values = [item[0]['i'] for item in driving_afe['afe']]
i_0_values = pd.DataFrame(i_0_values, columns=['i_0_0',
                                               'i_0_1',
                                               'i_0_2',
                                               'i_0_3'])

m_0_values = [item[0]['m'][0] for item in driving_afe['afe']]
m_0_values = pd.DataFrame(m_0_values, columns=['m_0_0',
                                               'm_0_1',
                                               'm_0_2',
                                               'm_0_3',
                                               'm_0_4',
                                               'm_0_5',
                                               'm_0_6',
                                               'm_0_7']).drop(['m_0_6', 'm_0_7'], axis=1)

# Right eye data
i_1_values = [item[1]['i'] for item in driving_afe['afe']]
i_1_values = pd.DataFrame(i_1_values, columns=['i_1_0',
                                               'i_1_1',
                                               'i_1_2',
                                               'i_1_3'])

m_1_values = [item[0]['m'][0] for item in driving_afe['afe']]
m_1_values = pd.DataFrame(m_1_values, columns=['m_1_0',
                                               'm_1_1',
                                               'm_1_2',
                                               'm_1_3',
                                               'm_1_4',
                                               'm_1_5',
                                               'm_1_6',
                                               'm_1_7']).drop(['m_1_6', 'm_1_7'], axis=1)

driving_afe = pd.concat([driving_afe.reset_index(drop=True),
                         i_0_values.reset_index(drop=True),
                         m_0_values.reset_index(drop=True),
                         i_1_values.reset_index(drop=True),
                         m_1_values.reset_index(drop=True)], axis=1)
driving_afe.drop('afe', axis=1, inplace=True)

# Left eye data
i_0_values = [item[0]['i'] for item in indoor_afe['afe']]
i_0_values = pd.DataFrame(i_0_values, columns=['i_0_0',
                                               'i_0_1',
                                               'i_0_2',
                                               'i_0_3'])

m_0_values = [item[0]['m'][0] for item in indoor_afe['afe']]
m_0_values = pd.DataFrame(m_0_values, columns=['m_0_0',
                                               'm_0_1',
                                               'm_0_2',
                                               'm_0_3',
                                               'm_0_4',
                                               'm_0_5',
                                               'm_0_6',
                                               'm_0_7']).drop(['m_0_6', 'm_0_7'], axis=1)

# Right eye data
i_1_values = [item[1]['i'] for item in indoor_afe['afe']]
i_1_values = pd.DataFrame(i_1_values, columns=['i_1_0',
                                               'i_1_1',
                                               'i_1_2',
                                               'i_1_3'])

m_1_values = [item[0]['m'][0] for item in indoor_afe['afe']]
m_1_values = pd.DataFrame(m_1_values, columns=['m_1_0',
                                               'm_1_1',
                                               'm_1_2',
                                               'm_1_3',
                                               'm_1_4',
                                               'm_1_5',
                                               'm_1_6',
                                               'm_1_7']).drop(['m_1_6', 'm_1_7'], axis=1)

indoor_afe = pd.concat([indoor_afe.reset_index(drop=True),
                         i_0_values.reset_index(drop=True),
                         m_0_values.reset_index(drop=True),
                         i_1_values.reset_index(drop=True),
                         m_1_values.reset_index(drop=True)], axis=1)
indoor_afe.drop('afe', axis=1, inplace=True)

# Left eye data
i_0_values = [item[0]['i'] for item in walking_afe['afe']]
i_0_values = pd.DataFrame(i_0_values, columns=['i_0_0',
                                               'i_0_1',
                                               'i_0_2',
                                               'i_0_3'])

m_0_values = [item[0]['m'][0] for item in walking_afe['afe']]
m_0_values = pd.DataFrame(m_0_values, columns=['m_0_0',
                                               'm_0_1',
                                               'm_0_2',
                                               'm_0_3',
                                               'm_0_4',
                                               'm_0_5',
                                               'm_0_6',
                                               'm_0_7']).drop(['m_0_6', 'm_0_7'], axis=1)

# Right eye data
i_1_values = [item[1]['i'] for item in walking_afe['afe']]
i_1_values = pd.DataFrame(i_1_values, columns=['i_1_0',
                                               'i_1_1',
                                               'i_1_2',
                                               'i_1_3'])

m_1_values = [item[0]['m'][0] for item in walking_afe['afe']]
m_1_values = pd.DataFrame(m_1_values, columns=['m_1_0',
                                               'm_1_1',
                                               'm_1_2',
                                               'm_1_3',
                                               'm_1_4',
                                               'm_1_5',
                                               'm_1_6',
                                               'm_1_7']).drop(['m_1_6', 'm_1_7'], axis=1)

walking_afe = pd.concat([walking_afe.reset_index(drop=True),
                         i_0_values.reset_index(drop=True),
                         m_0_values.reset_index(drop=True),
                         i_1_values.reset_index(drop=True),
                         m_1_values.reset_index(drop=True)], axis=1)
walking_afe.drop('afe', axis=1, inplace=True)

driving_afe_mean = [driving_afe['m_0_0'].mean(),
                    driving_afe['m_0_1'].mean(),
                    driving_afe['m_0_2'].mean(),
                    driving_afe['m_0_3'].mean(),
                    driving_afe['m_0_4'].mean(),
                    driving_afe['m_0_5'].mean()]
plt.bar(x=['m_0_0', 'm_0_1', 'm_0_2', 'm_0_3', 'm_0_4', 'm_0_5'], height=driving_afe_mean)
plt.title('Mean of Driving AFE Data')
plt.savefig('./figures/driving_afe_mean.png')

table = driving_afe
        #   , indoor_afe, walking_afe]
cols = ['m_0_0', 'm_0_1', 'm_0_2', 'm_0_3', 'm_0_4', 'm_0_5']
fig = plt.figure(figsize=[50, 50])

for i in range(len(cols)):
    col = cols[i]
    x = []
    y = []
    m = table[col].mean()
    for j in range(table.shape[0]):
        x.append(j)
        y.append(m)

    ax = plt.subplot(10, 10, i + 1)
    ax.plot(table[col])

    ax1 = plt.subplot(10, 10, i + 1)
    ax.set_title(col)
    # ax.axis('off')
    # for i in range(5):

fig.savefig('./figures/driving_afe_values.png')

driving_imu_x = driving_imu['v'].apply(lambda x: x[0])
driving_imu_x = driving_imu_x.reset_index(drop=True)

driving_imu_y = driving_imu['v'].apply(lambda x: x[1])
driving_imu_y = driving_imu_y.reset_index(drop=True)

driving_imu_z = driving_imu['v'].apply(lambda x: x[2])
driving_imu_z = driving_imu_z.reset_index(drop=True)

driving_imu_x_angle = []
for i in range(len(driving_imu_x)):
    driving_imu_x_angle.append(np.arctan(driving_imu_x[i]/driving_imu_y[i]) / np.pi * 180)

driving_imu_x_angle = pd.DataFrame(driving_imu_x_angle)

driving_imu_z_angle = []
for i in range(len(driving_imu_z)):
    driving_imu_z_angle.append(np.arctan(driving_imu_z[i]/driving_imu_y[i]) / np.pi * 180)

driving_imu_z_angle = pd.DataFrame(driving_imu_z_angle)

minimized_x_angle = driving_imu_x_angle[0:4000]
minimized_z_angle = driving_imu_z_angle[0:4000]

minimized_x_angle.reset_index(drop=True)[0].to_csv('minimized_x_angle.csv')
minimized_z_angle.reset_index(drop=True)[0].to_csv('minimized_z_angle.csv')

minimized_x_angle.reset_index(drop=True)[0].to_json('../data/minimized_x_angle.json')
minimized_z_angle.reset_index(drop=True)[0].to_json('../data/minimized_z_angle.json')

driving_p1_imu_x = []
driving_p1_imu_y = []
driving_p1_imu_z = []
for i in driving_p1_imu:
    for j in range(len(i['v'])):
        driving_p1_imu_x.append(i['v'].iloc[j][0])
        driving_p1_imu_y.append(i['v'].iloc[j][1])
        driving_p1_imu_z.append(i['v'].iloc[j][2])

driving_p1_imu_x_angle = []
for i in range(len(driving_p1_imu_x)):
    driving_p1_imu_x_angle.append(np.arctan(driving_p1_imu_x[i]/driving_p1_imu_y[i]) / np.pi * 180)

driving_p1_imu_x_angle = pd.DataFrame(driving_p1_imu_x_angle)

driving_p1_imu_z_angle = []
for i in range(len(driving_p1_imu_z)):
    driving_p1_imu_z_angle.append(np.arctan(driving_p1_imu_z[i]/driving_p1_imu_y[i]) / np.pi * 180)

driving_p1_imu_z_angle = pd.DataFrame(driving_p1_imu_z_angle)

driving_p1_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_driving_p1_imu_x_angle.json')
driving_p1_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_driving_p1_imu_z_angle.json')

driving_p2_imu_x = []
driving_p2_imu_y = []
driving_p2_imu_z = []
for i in driving_p2_imu:
    for j in range(len(i['v'])):
        driving_p2_imu_x.append(i['v'].iloc[j][0])
        driving_p2_imu_y.append(i['v'].iloc[j][1])
        driving_p2_imu_z.append(i['v'].iloc[j][2])
driving_p2_imu_x_angle = []
for i in range(len(driving_p2_imu_x)):
    driving_p2_imu_x_angle.append(np.arctan(driving_p2_imu_x[i]/driving_p2_imu_y[i]) / np.pi * 180)

driving_p2_imu_x_angle = pd.DataFrame(driving_p2_imu_x_angle)
driving_p2_imu_z_angle = []
for i in range(len(driving_p2_imu_z)):
    driving_p2_imu_z_angle.append(np.arctan(driving_p2_imu_z[i]/driving_p2_imu_y[i]) / np.pi * 180)

driving_p2_imu_z_angle = pd.DataFrame(driving_p2_imu_z_angle)
driving_p2_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_driving_p2_imu_x_angle.json')
driving_p2_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_driving_p2_imu_z_angle.json')

driving_p3_imu_x = []
driving_p3_imu_y = []
driving_p3_imu_z = []
for i in driving_p3_imu:
    for j in range(len(i['v'])):
        driving_p3_imu_x.append(i['v'].iloc[j][0])
        driving_p3_imu_y.append(i['v'].iloc[j][1])
        driving_p3_imu_z.append(i['v'].iloc[j][2])
driving_p3_imu_x_angle = []
for i in range(len(driving_p3_imu_x)):
    driving_p3_imu_x_angle.append(np.arctan(driving_p3_imu_x[i]/driving_p3_imu_y[i]) / np.pi * 180)

driving_p3_imu_x_angle = pd.DataFrame(driving_p3_imu_x_angle)
driving_p3_imu_z_angle = []
for i in range(len(driving_p3_imu_z)):
    driving_p3_imu_z_angle.append(np.arctan(driving_p3_imu_z[i]/driving_p3_imu_y[i]) / np.pi * 180)

driving_p3_imu_z_angle = pd.DataFrame(driving_p3_imu_z_angle)
driving_p3_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_driving_p3_imu_x_angle.json')
driving_p3_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_driving_p3_imu_z_angle.json')

indoor_p1_imu_x = []
indoor_p1_imu_y = []
indoor_p1_imu_z = []
for i in indoor_p1_imu:
    for j in range(len(i['v'])):
        indoor_p1_imu_x.append(i['v'].iloc[j][0])
        indoor_p1_imu_y.append(i['v'].iloc[j][1])
        indoor_p1_imu_z.append(i['v'].iloc[j][2])
indoor_p1_imu_x_angle = []
for i in range(len(indoor_p1_imu_x)):
    indoor_p1_imu_x_angle.append(np.arctan(indoor_p1_imu_x[i]/indoor_p1_imu_y[i]) / np.pi * 180)

indoor_p1_imu_x_angle = pd.DataFrame(indoor_p1_imu_x_angle)
indoor_p1_imu_z_angle = []
for i in range(len(indoor_p1_imu_z)):
    indoor_p1_imu_z_angle.append(np.arctan(indoor_p1_imu_z[i]/indoor_p1_imu_y[i]) / np.pi * 180)

indoor_p1_imu_z_angle = pd.DataFrame(indoor_p1_imu_z_angle)
indoor_p1_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_indoor_p1_imu_x_angle.json')
indoor_p1_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_indoor_p1_imu_z_angle.json')

indoor_p2_imu_x = []
indoor_p2_imu_y = []
indoor_p2_imu_z = []
for i in indoor_p2_imu:
    for j in range(len(i['v'])):
        indoor_p2_imu_x.append(i['v'].iloc[j][0])
        indoor_p2_imu_y.append(i['v'].iloc[j][1])
        indoor_p2_imu_z.append(i['v'].iloc[j][2])
indoor_p2_imu_x_angle = []
for i in range(len(indoor_p2_imu_x)):
    indoor_p2_imu_x_angle.append(np.arctan(indoor_p2_imu_x[i]/indoor_p2_imu_y[i]) / np.pi * 180)

indoor_p2_imu_x_angle = pd.DataFrame(indoor_p2_imu_x_angle)
indoor_p2_imu_z_angle = []
for i in range(len(indoor_p2_imu_z)):
    indoor_p2_imu_z_angle.append(np.arctan(indoor_p2_imu_z[i]/indoor_p2_imu_y[i]) / np.pi * 180)

indoor_p2_imu_z_angle = pd.DataFrame(indoor_p2_imu_z_angle)
indoor_p2_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_indoor_p2_imu_x_angle.json')
indoor_p2_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_indoor_p2_imu_z_angle.json')

indoor_p3_imu_x = []
indoor_p3_imu_y = []
indoor_p3_imu_z = []
for i in indoor_p3_imu:
    for j in range(len(i['v'])):
        indoor_p3_imu_x.append(i['v'].iloc[j][0])
        indoor_p3_imu_y.append(i['v'].iloc[j][1])
        indoor_p3_imu_z.append(i['v'].iloc[j][2])
indoor_p3_imu_x_angle = []
for i in range(len(indoor_p3_imu_x)):
    indoor_p3_imu_x_angle.append(np.arctan(indoor_p3_imu_x[i]/indoor_p3_imu_y[i]) / np.pi * 180)

indoor_p3_imu_x_angle = pd.DataFrame(indoor_p3_imu_x_angle)
indoor_p3_imu_z_angle = []
for i in range(len(indoor_p3_imu_z)):
    indoor_p3_imu_z_angle.append(np.arctan(indoor_p3_imu_z[i]/indoor_p3_imu_y[i]) / np.pi * 180)

indoor_p3_imu_z_angle = pd.DataFrame(indoor_p3_imu_z_angle)
indoor_p3_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_indoor_p3_imu_x_angle.json')
indoor_p3_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_indoor_p3_imu_z_angle.json')

walking_p1_imu_x = []
walking_p1_imu_y = []
walking_p1_imu_z = []
for i in walking_p1_imu:
    for j in range(len(i['v'])):
        walking_p1_imu_x.append(i['v'].iloc[j][0])
        walking_p1_imu_y.append(i['v'].iloc[j][1])
        walking_p1_imu_z.append(i['v'].iloc[j][2])
walking_p1_imu_x_angle = []
for i in range(len(walking_p1_imu_x)):
    walking_p1_imu_x_angle.append(np.arctan(walking_p1_imu_x[i]/walking_p1_imu_y[i]) / np.pi * 180)

walking_p1_imu_x_angle = pd.DataFrame(walking_p1_imu_x_angle)
walking_p1_imu_z_angle = []
for i in range(len(walking_p1_imu_z)):
    walking_p1_imu_z_angle.append(np.arctan(walking_p1_imu_z[i]/walking_p1_imu_y[i]) / np.pi * 180)

walking_p1_imu_z_angle = pd.DataFrame(walking_p1_imu_z_angle)
walking_p1_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_walking_p1_imu_x_angle.json')
walking_p1_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_walking_p1_imu_z_angle.json')

walking_p2_imu_x = []
walking_p2_imu_y = []
walking_p2_imu_z = []
for i in walking_p2_imu:
    for j in range(len(i['v'])):
        walking_p2_imu_x.append(i['v'].iloc[j][0])
        walking_p2_imu_y.append(i['v'].iloc[j][1])
        walking_p2_imu_z.append(i['v'].iloc[j][2])
walking_p2_imu_x_angle = []
for i in range(len(walking_p2_imu_x)):
    walking_p2_imu_x_angle.append(np.arctan(walking_p2_imu_x[i]/walking_p2_imu_y[i]) / np.pi * 180)

walking_p2_imu_x_angle = pd.DataFrame(walking_p2_imu_x_angle)
walking_p2_imu_z_angle = []
for i in range(len(walking_p2_imu_z)):
    walking_p2_imu_z_angle.append(np.arctan(walking_p2_imu_z[i]/walking_p2_imu_y[i]) / np.pi * 180)

walking_p2_imu_z_angle = pd.DataFrame(walking_p2_imu_z_angle)
walking_p2_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_walking_p2_imu_x_angle.json')
walking_p2_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_walking_p2_imu_z_angle.json')

walking_p3_imu_x = []
walking_p3_imu_y = []
walking_p3_imu_z = []
for i in walking_p3_imu:
    for j in range(len(i['v'])):
        walking_p3_imu_x.append(i['v'].iloc[j][0])
        walking_p3_imu_y.append(i['v'].iloc[j][1])
        walking_p3_imu_z.append(i['v'].iloc[j][2])
walking_p3_imu_x_angle = []
for i in range(len(walking_p3_imu_x)):
    walking_p3_imu_x_angle.append(np.arctan(walking_p3_imu_x[i]/walking_p3_imu_y[i]) / np.pi * 180)

walking_p3_imu_x_angle = pd.DataFrame(walking_p3_imu_x_angle)
walking_p3_imu_z_angle = []
for i in range(len(walking_p3_imu_z)):
    walking_p3_imu_z_angle.append(np.arctan(walking_p3_imu_z[i]/walking_p3_imu_y[i]) / np.pi * 180)

walking_p3_imu_z_angle = pd.DataFrame(walking_p3_imu_z_angle)
walking_p3_imu_x_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_walking_p3_imu_x_angle.json')
walking_p3_imu_z_angle.reset_index(drop=True)[0:4000][0].to_json('../data/minimized_walking_p3_imu_z_angle.json')

scenarios = [[driving_imu_x_angle, driving_imu_z_angle],
             [indoor_p1_imu_x_angle, indoor_p1_imu_z_angle],
             [walking_p1_imu_x_angle, indoor_p1_imu_z_angle]]

labels = [['driving_x', 'driving_z'], ['indoor_x', 'indoor_z'], ['walking_x', 'walking_z']]

figure, axs = plt.subplots(3, 2, figsize=(15, 15))

for i in range(len(scenarios)):
    scenario = scenarios[i]
    label = labels[i]
    for j in range(len(scenario)):
        axs[i, j].plot(scenario[j])
        axs[i, j].set_title(label[j])

figure.savefig('./figures/scenarios.png')

def is_tired(dataframe, col, threshold):
    return dataframe[col].mean() > threshold

tired_threshold = 7

minimized_indoor_p1_imu_x_angle = abs(indoor_p1_imu_x_angle)
minimized_indoor_p1_imu_z_angle = abs(indoor_p1_imu_z_angle)

minimized_indoor_p2_imu_x_angle = abs(indoor_p2_imu_x_angle)
minimized_indoor_p2_imu_z_angle = abs(indoor_p2_imu_z_angle)

minimized_indoor_p3_imu_x_angle = abs(indoor_p3_imu_x_angle)
minimized_indoor_p3_imu_z_angle = abs(indoor_p3_imu_z_angle)

indoor_p1 = pd.DataFrame({'p1': minimized_indoor_p1_imu_z_angle[0]})
indoor_p1.reset_index(drop=True)['p1'].to_json('../data/istired_indoor_p1.json')

indoor_p2 = pd.DataFrame({'p2': minimized_indoor_p2_imu_z_angle[0]})
indoor_p2.reset_index(drop=True)['p2'].to_json('../data/istired_indoor_p2.json')

indoor_p3 = pd.DataFrame({'p3': minimized_indoor_p3_imu_z_angle[0]})
indoor_p3.reset_index(drop=True)['p3'].to_json('../data/istired_indoor_p3.json')

minimized_driving_p1_imu_x_angle = abs(driving_p1_imu_x_angle)
minimized_driving_p1_imu_z_angle = abs(driving_p1_imu_z_angle)

minimized_driving_p2_imu_x_angle = abs(driving_p2_imu_x_angle)
minimized_driving_p2_imu_z_angle = abs(driving_p2_imu_z_angle)

minimized_driving_p3_imu_x_angle = abs(driving_p3_imu_x_angle)
minimized_driving_p3_imu_z_angle = abs(driving_p3_imu_z_angle)

driving_p1 = pd.DataFrame({'p1': minimized_driving_p1_imu_z_angle[0]})
driving_p1.reset_index(drop=True)['p1'].to_json('../data/istired_driving_p1.json')

driving_p2 = pd.DataFrame({'p2': minimized_driving_p2_imu_z_angle[0]})
driving_p2.reset_index(drop=True)['p2'].to_json('../data/istired_driving_p2.json')

driving_p3 = pd.DataFrame({'p3': minimized_driving_p3_imu_z_angle[0]})
driving_p3.reset_index(drop=True)['p3'].to_json('../data/istired_driving_p3.json')

minimized_walking_p1_imu_x_angle = abs(walking_p1_imu_x_angle)
minimized_walking_p1_imu_z_angle = abs(walking_p1_imu_z_angle)

minimized_walking_p2_imu_x_angle = abs(walking_p2_imu_x_angle)
minimized_walking_p2_imu_z_angle = abs(walking_p2_imu_z_angle)

minimized_walking_p3_imu_x_angle = abs(walking_p3_imu_x_angle)
minimized_walking_p3_imu_z_angle = abs(walking_p3_imu_z_angle)

walking_p1 = pd.DataFrame({'p1': minimized_walking_p1_imu_z_angle[0]})
walking_p1.reset_index(drop=True)['p1'].to_json('../data/istired_walking_p1.json')

walking_p2 = pd.DataFrame({'p2': minimized_walking_p2_imu_z_angle[0]})
walking_p2.reset_index(drop=True)['p2'].to_json('../data/istired_walking_p2.json')

walking_p3 = pd.DataFrame({'p3': minimized_walking_p3_imu_z_angle[0]})
walking_p3.reset_index(drop=True)['p3'].to_json('../data/istired_walking_p3.json')