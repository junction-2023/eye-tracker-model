import pandas as pd
import matplotlib.pyplot as plt

file_path = 'eye_movement_saccades_fixations.csv'

df = pd.read_csv(file_path)

plt.figure(figsize=(12, 6))

plt.subplot(2, 1, 1)
plt.plot(df['ticktime'], df['left_saccade'], label='Left Saccade', color='blue')
plt.plot(df['ticktime'], df['left_fixation'], label='Left Fixation', color='green')
plt.fill_between(df['ticktime'], df['left_fixation'], where=df['left_fixation'], color='green', alpha=0.3)
plt.xlabel('Time (Ticktime)')
plt.ylabel('Saccade/Fixation')
plt.title('Left Eye Saccades and Fixations')
plt.legend()

plt.subplot(2, 1, 2)
plt.plot(df['ticktime'], df['left_fixation_duration'], label='Left Fixation Duration', color='red')
plt.xlabel('Time (Ticktime)')
plt.ylabel('Duration (ms)')
plt.title('Left Eye Fixation Duration')
plt.legend()

plt.tight_layout()
plt.show()
