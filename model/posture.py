import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

with open('IMU_000_CONFIDENTIAL.json', 'r') as imu_file:
    imu_data = json.load(imu_file)

imu_df = pd.DataFrame([{
    'ticktime': item['i'][0],
    'acc_x': item['v'][0],
    'acc_y': item['v'][1],
    'acc_z': item['v'][2]
} for item in imu_data])

# Calculate the tilt angles in degrees based on the accelerometer data
imu_df['tilt_x'] = np.arctan2(imu_df['acc_x'], np.sqrt(imu_df['acc_y']**2 + imu_df['acc_z']**2)) * (180 / np.pi)
imu_df['tilt_y'] = np.arctan2(imu_df['acc_y'], np.sqrt(imu_df['acc_x']**2 + imu_df['acc_z']**2)) * (180 / np.pi)
imu_df['tilt_z'] = np.arctan2(np.sqrt(imu_df['acc_x']**2 + imu_df['acc_y']**2), imu_df['acc_z']) * (180 / np.pi)

imu_df.to_csv('head_posture_estimation.csv', index=False)
print("Head posture estimation saved to head_posture_estimation.csv")

plt.figure(figsize=(15, 5))

plt.subplot(1, 3, 1)
plt.plot(imu_df['ticktime'], imu_df['tilt_x'], label='Tilt X')
plt.xlabel('Ticktime')
plt.ylabel('Tilt Angle (degrees)')
plt.title('Tilt X Over Time')
plt.legend()

plt.subplot(1, 3, 2)
plt.plot(imu_df['ticktime'], imu_df['tilt_y'], label='Tilt Y')
plt.xlabel('Ticktime')
plt.ylabel('Tilt Angle (degrees)')
plt.title('Tilt Y Over Time')
plt.legend()

plt.subplot(1, 3, 3)
plt.plot(imu_df['ticktime'], imu_df['tilt_z'], label='Tilt Z')
plt.xlabel('Ticktime')
plt.ylabel('Tilt Angle (degrees)')
plt.title('Tilt Z Over Time')
plt.legend()

plt.tight_layout()
plt.show()
