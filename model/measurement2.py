import json
import csv

def read_json_file(file_path):
    with open(file_path, 'r') as file:
        return json.load(file)

def combine_data(afe_data, imu_data):
    combined_data_dict = {}

    for afe_entry in afe_data:
        ticktime = afe_entry["afe"][0]["i"][0] if afe_entry["afe"] else None
        if ticktime:
            combined_data_dict[ticktime] = {"afe": afe_entry}

    for imu_entry in imu_data:
        ticktime = imu_entry["i"][0]
        if ticktime in combined_data_dict:
            combined_data_dict[ticktime]["imu"] = imu_entry
        else:
            combined_data_dict[ticktime] = {"imu": imu_entry}

    return combined_data_dict

def analyze_saccades(combined_data):
    # Simple saccade detection algorithm
    for ticktime, data in combined_data.items():
        if "afe" in data and "m" in data["afe"]:
            # Considering only the first six values as per your instruction
            eye_signals = data["afe"]["m"][:6]
            # Compare each signal with the next to find large changes
            for i in range(len(eye_signals) - 1):
                # Example threshold for change, needs tuning for real data
                threshold = 1000 
                if abs(eye_signals[i] - eye_signals[i + 1]) > threshold:
                    data["afe"]["saccade_detected"] = True
                    break
            else:
                data["afe"]["saccade_detected"] = False

# Example usage
afe_data = read_json_file('AFE_000_CONFIDENTIAL.json')
imu_data = read_json_file('IMU_000_CONFIDENTIAL.json')

combined_data = combine_data(afe_data, imu_data)

# Performing saccade analysis
analyze_saccades(combined_data)

# Save the combined data with saccade analysis
with open('combined_data_with_saccades.json', 'w') as file:
    json.dump(combined_data, file, indent=4)


import csv

import csv

def analyze_saccades_and_output_csv(combined_data, output_csv_path):
    with open(output_csv_path, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['Ticktime', 'Saccade Detected'])

        for ticktime, data in combined_data.items():
            saccade_detected = False
            if "afe" in data and data["afe"]:
                # Iterate over each eye data in 'afe' array
                for eye_data in data["afe"]:
                    if eye_data and "m" in eye_data and eye_data["m"]:
                        # Assuming 'm' values are in the first element of the 'm' array
                        eye_signals = eye_data["m"][0]
                        # Compare each signal with the next to find large changes
                        for i in range(len(eye_signals) - 1):
                            threshold = 100  # Example threshold, needs tuning
                            if abs(eye_signals[i] - eye_signals[i + 1]) > threshold:
                                saccade_detected = True
                                break
                        if saccade_detected:
                            break

            csv_writer.writerow([ticktime, saccade_detected])

# Example usage
afe_data = read_json_file('AFE_000_CONFIDENTIAL.json')
imu_data = read_json_file('IMU_000_CONFIDENTIAL.json')

combined_data = combine_data(afe_data, imu_data)

# Output CSV with saccade analysis
output_csv_path = 'saccades_analysis.csv'
analyze_saccades_and_output_csv(combined_data, output_csv_path)

import json
import csv

def analyze_saccades(combined_data):
    saccade_results = []
    threshold = 1000  # Define a threshold for change to detect saccades

    for ticktime, data in combined_data.items():
        afe_data = data.get("afe", {}).get("afe", [])
        saccade_detected = False

        for eye_data in afe_data:
            if "m" in eye_data and eye_data["m"]:
                eye_signals = eye_data["m"][0]

                # Compare each signal with the next to find large changes
                for i in range(len(eye_signals) - 1):
                    if abs(eye_signals[i] - eye_signals[i + 1]) > threshold:
                        saccade_detected = True
                        break
            
            if saccade_detected:
                break

        saccade_results.append((ticktime, saccade_detected))

    return saccade_results

def output_to_csv(saccade_results, filename):
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Ticktime", "Saccade Detected"])

        for ticktime, saccade_detected in saccade_results:
            writer.writerow([ticktime, saccade_detected])

saccade_results = analyze_saccades(combined_data)

output_csv_file = "saccade_analysis2.csv"
output_to_csv(saccade_results, output_csv_file)
