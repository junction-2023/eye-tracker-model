import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json

with open('IMU_000_CONFIDENTIAL.json', 'r') as file:
    imu_data = json.load(file)

data_acc = {
    "ticktime": [d["i"][0] for d in imu_data],
    "x": [d["v"][0] for d in imu_data],
    "y": [d["v"][1] for d in imu_data],
    "z": [d["v"][2] for d in imu_data]
}
df_acc = pd.DataFrame(data_acc)

df_acc['magnitude'] = np.sqrt(df_acc['x']**2 + df_acc['y']**2 + df_acc['z']**2)
window_size = 10  # Example window size for rolling standard deviation
df_acc['stability'] = df_acc['magnitude'].rolling(window=window_size).std()


df_eye = pd.read_csv('eye_movement_saccades_fixations.csv')


combined_df = pd.merge_asof(df_acc.sort_values('ticktime'), df_eye.sort_values('ticktime'), on='ticktime')

correlation = combined_df[['stability', 'left_fixation_duration']].corr()
print("Correlation Matrix:\n", correlation)

plt.figure(figsize=(15, 6))

plt.subplot(2, 1, 1)
plt.plot(combined_df['ticktime'], combined_df['stability'], label='Accelerometer Stability', color='blue')
plt.xlabel('Time (Ticktime)')
plt.ylabel('Stability')
plt.title('Accelerometer Stability Over Time')
plt.legend()

plt.subplot(2, 1, 2)
plt.plot(combined_df['ticktime'], combined_df['left_fixation_duration'], label='Left Fixation Duration', color='green')
plt.xlabel('Time (Ticktime)')
plt.ylabel('Fixation Duration (ms)')
plt.title('Eye-Tracking Fixation Duration Over Time')
plt.legend()

plt.tight_layout()
plt.show()
