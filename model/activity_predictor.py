import pandas as pd
import json
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

'''A simple neural network to predict an activity based on eye movement data.'''


# Load and preprocess data. 
def load_and_preprocess_data(json_data, label):
    processed_data = []
    for record in json_data:
        x, y, z = record['v'][0], record['v'][1], record['v'][2]
        processed_data.append([x, y, z, label])
    return pd.DataFrame(processed_data, columns=['X', 'Y', 'Z', 'label'])

# Use separate files for different acivities. Modify these as needed.
with open('C:/Users/vaino/Downloads/drive-download-20231110T171508Z-001/Walking/Participant_1/IMU_000_CONFIDENTIAL.json') as file:
    walking_data = json.load(file)
with open('C:/Users/vaino/Downloads/drive-download-20231110T171508Z-001/Indoor/Participant_1/IMU_000_CONFIDENTIAL.json') as file:
    reading_data = json.load(file)
with open('C:/Users/vaino/Downloads/drive-download-20231110T171508Z-001/Driving/Participant_1/IMU_000_CONFIDENTIAL.json') as file:
    driving_data = json.load(file)

# Process and label each dataset
df_walking = load_and_preprocess_data(walking_data, 0)
df_reading = load_and_preprocess_data(reading_data, 1)
df_driving = load_and_preprocess_data(driving_data, 2)

# Combine into one DataFrame.
df_combined = pd.concat([df_walking, df_reading, df_driving], ignore_index=True)

# Normalize 
scaler = StandardScaler()
features = scaler.fit_transform(df_combined[['X', 'Y', 'Z']])
labels = df_combined['label'].values

# Train-test-split.
X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)

class CustomDataset(Dataset):
    def __init__(self, features, labels):
        self.features = features
        self.labels = labels

    def __len__(self):
        return len(self.features)

    def __getitem__(self, idx):
        return self.features[idx], self.labels[idx]

# The model itself
class ActivityPredictor(nn.Module):
    def __init__(self):
        super(ActivityPredictor, self).__init__()
        self.fc1 = nn.Linear(3, 64)
        self.fc2 = nn.Linear(64, 3)  # Output size is 3 for three classes

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = self.fc2(x)  # No sigmoid activation, using raw output for CrossEntropyLoss
        return x

# Loaders
train_dataset = CustomDataset(torch.tensor(X_train, dtype=torch.float32), torch.tensor(y_train, dtype=torch.long))
test_dataset = CustomDataset(torch.tensor(X_test, dtype=torch.float32), torch.tensor(y_test, dtype=torch.long))
train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True)
test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False)


model = ActivityPredictor()

# Calculate loss & optimizer.
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)

# Training loop
num_epochs = 5  # 5 epoch for now
for epoch in range(num_epochs):
    for inputs, labels in train_loader:
        outputs = model(inputs)
        loss = criterion(outputs, labels)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')

# Save
torch.save(model.state_dict(), 'activity_model.pth')

# Evaluation loop
def evaluate_model(model, loader):
    correct = 0
    total = 0
    with torch.no_grad():
        for inputs, labels in loader:
            outputs = model(inputs)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    return correct / total


model.eval()
test_accuracy = evaluate_model(model, test_loader)
print(f'Accuracy on the test split: {test_accuracy * 100:.2f}%')

# Load a dataset for prediction
def load_and_preprocess_new_data(file_path, scaler):
    with open(file_path, 'r') as file:
        new_data = json.load(file)

    new_df = load_and_preprocess_data(new_data, label=None)  # Label is not needed for prediction
    scaled_features = scaler.transform(new_df[['X', 'Y', 'Z']])
    return scaled_features

# Get percentages from output
def predict_activity_and_calculate_percentages(model, file_path, scaler):
    features = load_and_preprocess_new_data(file_path, scaler)
    input_tensor = torch.tensor(features, dtype=torch.float32)

    class_counts = {'walking': 0, 'reading': 0, 'driving': 0}
    total_predictions = 0

    with torch.no_grad():
        outputs = model(input_tensor)
        _, predicted = torch.max(outputs, 1)

    for p in predicted:
        if p == 0:
            class_counts['walking'] += 1
        elif p == 1:
            class_counts['reading'] += 1
        else:  # p == 2
            class_counts['driving'] += 1
        total_predictions += 1

    for activity in class_counts:
        class_counts[activity] = (class_counts[activity] / total_predictions) * 100

    return class_counts

model = ActivityPredictor()
model.load_state_dict(torch.load('activity_model.pth'))
model.eval()

new_file_path = 'C:/Users/vaino/Downloads/drive-download-20231110T171508Z-001/Driving/Participant_1/IMU_000_CONFIDENTIAL.json' 
activity_percentages = predict_activity_and_calculate_percentages(model, new_file_path, scaler)

print("Activity percentages in the new dataset:")
for activity, percentage in activity_percentages.items():
    print(f"{activity}: {percentage:.2f}%")
