import json
import pandas as pd
import numpy as np

with open('combined_data.json', 'r') as file:
    combined_data = json.load(file)

df = pd.DataFrame(combined_data)

df['left_eye_x'] = df['afe_data'].apply(lambda x: x[0]['m'][0][0] if x and 'm' in x[0] and len(x[0]['m']) > 0 else np.nan)
df['left_eye_y'] = df['afe_data'].apply(lambda x: x[0]['m'][0][1] if x and 'm' in x[0] and len(x[0]['m']) > 0 else np.nan)
df['right_eye_x'] = df['afe_data'].apply(lambda x: x[1]['m'][0][0] if len(x) > 1 and 'm' in x[1] and len(x[1]['m']) > 0 else np.nan)
df['right_eye_y'] = df['afe_data'].apply(lambda x: x[1]['m'][0][1] if len(x) > 1 and 'm' in x[1] and len(x[1]['m']) > 0 else np.nan)

df['frame_duration'] = df['ticktime'].diff().fillna(0)

window_size = 5  # Example window size for smoothing
df['left_eye_x_smooth'] = df['left_eye_x'].rolling(window=window_size, min_periods=1).mean()
df['left_eye_y_smooth'] = df['left_eye_y'].rolling(window=window_size, min_periods=1).mean()
df['right_eye_x_smooth'] = df['right_eye_x'].rolling(window=window_size, min_periods=1).mean()
df['right_eye_y_smooth'] = df['right_eye_y'].rolling(window=window_size, min_periods=1).mean()

df['left_eye_vel'] = np.sqrt(np.square(df['left_eye_x_smooth'].diff()) + np.square(df['left_eye_y_smooth'].diff())) / df['frame_duration']
df['right_eye_vel'] = np.sqrt(np.square(df['right_eye_x_smooth'].diff()) + np.square(df['right_eye_y_smooth'].diff())) / df['frame_duration']

# Experiment with different thresholds for saccade detection
saccade_velocity_threshold = 0.5  # Adjust as necessary

# Identify saccades based on velocity
df['left_saccade'] = df['left_eye_vel'] > saccade_velocity_threshold
df['right_saccade'] = df['right_eye_vel'] > saccade_velocity_threshold

# Identify fixations as periods without saccades
df['left_fixation'] = ~df['left_saccade']
df['right_fixation'] = ~df['right_saccade']

# Group consecutive fixation frames and sum their durations
df['left_fixation_group'] = (df['left_fixation'] != df['left_fixation'].shift()).cumsum()
df['right_fixation_group'] = (df['right_fixation'] != df['right_fixation'].shift()).cumsum()
df['left_fixation_duration'] = df.groupby('left_fixation_group')['frame_duration'].transform('sum') * df['left_fixation']
df['right_fixation_duration'] = df.groupby('right_fixation_group')['frame_duration'].transform('sum') * df['right_fixation']

output_df = df[['ticktime', 'left_saccade', 'right_saccade', 'left_fixation', 'left_fixation_duration', 'right_fixation', 'right_fixation_duration']]

output_df.to_csv('eye_movement_saccades_fixations.csv', index=False)

print("Processed data saved to 'eye_movement_saccades_fixations.csv'")
