import json
import pandas as pd

def determine_gaze_direction(sensor_values):
    flat_sensor_values = [val for sublist in sensor_values for val in sublist]

    left_eye = flat_sensor_values[:3]
    right_eye = flat_sensor_values[3:6]

    if sum(left_eye) < sum(right_eye):
        horizontal = "left"
    else:
        horizontal = "right"


    if flat_sensor_values[0] + flat_sensor_values[3] < flat_sensor_values[2] + flat_sensor_values[5]:
        vertical = "up"
    else:
        vertical = "down"

    return f"{vertical}-{horizontal}"

with open('AFE_000_CONFIDENTIAL.json', 'r') as file:
    afe_data = json.load(file)

output = []
for frame in afe_data:
    if 'afe' in frame and frame['afe']:
        for eye_data in frame['afe']:
            if 'm' in eye_data and eye_data['m']:
                gaze_direction = determine_gaze_direction(eye_data['m'][:6])
                output.append({
                    'ticktime': eye_data['i'][0],
                    'gaze_direction': gaze_direction
                })

output_df = pd.DataFrame(output)
output_df.to_csv('gaze_directions.csv', index=False)
