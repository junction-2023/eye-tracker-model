import os, time
from influxdb_client_3 import InfluxDBClient3, Point

token = os.environ.get("INFLUXDB_TOKEN")
org = "test"
host = "https://us-east-1-1.aws.cloud2.influxdata.com"

 
INFLUXDB_TOKEN="hWv97EGCJ8sHomw1K71K08EE7DTjGNIbZMnzJhYUsLKrglr8o8HK8yKlY76RU46NMgHx-N-BlB4EmPK4Gp-naQ=="
client = InfluxDBClient3(host=host, token=INFLUXDB_TOKEN, org=org)
database="test"

data = {
  "point1": {
    "location": "Klamath",
    "species": "bees",
    "count": 23,
  },
  "point2": {
    "location": "Portland",
    "species": "ants",
    "count": 30,
  },
  "point3": {
    "location": "Klamath",
    "species": "bees",
    "count": 28,
  },
  "point4": {
    "location": "Portland",
    "species": "ants",
    "count": 32,
  },
  "point5": {
    "location": "Klamath",
    "species": "bees",
    "count": 29,
  },
  "point6": {
    "location": "Portland",
    "species": "ants",
    "count": 40,
  },
}

for key in data:
  point = (
    Point("census")
    .tag("location", data[key]["location"])
    .field(data[key]["species"], data[key]["count"])
  )
  client.write(database=database, record=point)
  time.sleep(1) # separate points by 1 second

print("Complete. Return to the InfluxDB UI.")


query = """SELECT *
FROM 'census'
WHERE time >= now() - interval '24 hours'
AND ('bees' IS NOT NULL OR 'ants' IS NOT NULL)"""

# Execute the query
table = client.query(query=query, database="test", language='sql') )

# Convert to dataframe
df = table.to_pandas().sort_values(by="time")
print(df)
