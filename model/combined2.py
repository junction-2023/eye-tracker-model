import json

def combine_data(afe_data, imu_data):
    combined_data_dict = {}

    for afe_entry in afe_data:
        ticktime = afe_entry["afe"][0]["i"][0] if afe_entry["afe"] else None
        if ticktime:
            combined_data_dict[ticktime] = {"afe": afe_entry}

    for imu_entry in imu_data:
        ticktime = imu_entry["i"][0]
        if ticktime in combined_data_dict:
            combined_data_dict[ticktime]["imu"] = imu_entry
        else:
            combined_data_dict[ticktime] = {"imu": imu_entry}

    return combined_data_dict

def read_json_file(file_path):
    with open(file_path, 'r') as file:
        return json.load(file)

def write_json_file(data, file_path):
    with open(file_path, 'w') as file:
        json.dump(data, file, indent=4)

afe_data = read_json_file('AFE_000_CONFIDENTIAL.json')
imu_data = read_json_file('IMU_000_CONFIDENTIAL.json')

combined_data = combine_data(afe_data, imu_data)
write_json_file(combined_data, 'combined_data.json')
