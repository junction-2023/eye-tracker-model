import json
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Assuming imu_data_string contains your JSON data as a string
imu_file = open("IMU_000_CONFIDENTIAL.json", 'r')


imu_data = json.load(imu_file)

data = {
    "ticktime": [d["i"][0] for d in imu_data],
    "x": [d["v"][0] for d in imu_data],
    "y": [d["v"][1] for d in imu_data],
    "z": [d["v"][2] for d in imu_data]
}

df = pd.DataFrame(data)

df['magnitude'] = np.sqrt(df['x']**2 + df['y']**2 + df['z']**2)
window_size = 10  
df['stability'] = df['magnitude'].rolling(window=window_size).std()

plt.figure(figsize=(15, 6))

plt.subplot(2, 1, 1)
plt.plot(df['ticktime'], df['magnitude'], label='Magnitude of Acceleration', color='blue')
plt.xlabel('Time (Ticktime)')
plt.ylabel('Magnitude')
plt.title('Magnitude of Acceleration Over Time')
plt.legend()

plt.subplot(2, 1, 2)
plt.plot(df['ticktime'], df['stability'], label='Stability (Std Dev of Magnitude)', color='red')
plt.xlabel('Time (Ticktime)')
plt.ylabel('Stability')
plt.title('Stability (Stillness) Over Time')
plt.legend()

plt.tight_layout()
plt.show()
