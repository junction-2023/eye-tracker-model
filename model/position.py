import json
import pandas as pd
import numpy as np

with open('IMU_000_CONFIDENTIAL.json', 'r') as file:
    imu_data = json.load(file)

data = {
    "ticktime": [item["i"][0] for item in imu_data],
    "acc_x": [item["v"][0] for item in imu_data],
    "acc_y": [item["v"][1] for item in imu_data],
    "acc_z": [item["v"][2] for item in imu_data]
}
df = pd.DataFrame(data)

df['ticktime'] = df['ticktime'] / 1000.0

df['time_diff'] = df['ticktime'].diff().fillna(0)

df['vel_x'] = 0.0
df['vel_y'] = 0.0
df['vel_z'] = 0.0
df['pos_x'] = 0.0
df['pos_y'] = 0.0
df['pos_z'] = 0.0

for i in range(1, len(df)):
    df.loc[i, 'vel_x'] = df.loc[i-1, 'vel_x'] + df.loc[i, 'acc_x'] * df.loc[i, 'time_diff']
    df.loc[i, 'vel_y'] = df.loc[i-1, 'vel_y'] + df.loc[i, 'acc_y'] * df.loc[i, 'time_diff']
    df.loc[i, 'vel_z'] = df.loc[i-1, 'vel_z'] + df.loc[i, 'acc_z'] * df.loc[i, 'time_diff']

    df.loc[i, 'pos_x'] = df.loc[i-1, 'pos_x'] + df.loc[i, 'vel_x'] * df.loc[i, 'time_diff']
    df.loc[i, 'pos_y'] = df.loc[i-1, 'pos_y'] + df.loc[i, 'vel_y'] * df.loc[i, 'time_diff']
    df.loc[i, 'pos_z'] = df.loc[i-1, 'pos_z'] + df.loc[i, 'vel_z'] * df.loc[i, 'time_diff']

output_df = df[['ticktime', 'pos_x', 'pos_y', 'pos_z']]

output_df.to_csv('imu_positions.csv', index=False)
print("Data saved to imu_positions.csv")
