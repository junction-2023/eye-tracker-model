import json
import pandas as pd

# Load JSON files
with open('IMU_000_CONFIDENTIAL.json', 'r') as file:
    imu_data = json.load(file)
with open('AFE_000_CONFIDENTIAL.json', 'r') as file:
    afe_data = json.load(file)

# Flatten afe_data
flattened_afe_data = []
for entry in afe_data:
    ticktime = entry.get('afe', [{}])[0].get('i', [None])[0]
    if ticktime is not None:
        flattened_afe_data.append({
            'ticktime': ticktime,
            'afe_data': entry.get('afe', []),
            'other_data': {k: v for k, v in entry.items() if k != 'afe'}
        })

# Convert to pandas DataFrame
df_imu_data = pd.DataFrame([{'ticktime': d['i'][0], 'imu_data': d} for d in imu_data])
df_afe_data = pd.DataFrame(flattened_afe_data)

# Ensure ticktime is treated as integer for merging
df_imu_data['ticktime'] = pd.to_numeric(df_imu_data['ticktime'], errors='coerce')
df_afe_data['ticktime'] = pd.to_numeric(df_afe_data['ticktime'], errors='coerce')

# Sort dataframes by ticktime
df_imu_data = df_imu_data.sort_values(by='ticktime')
df_afe_data = df_afe_data.sort_values(by='ticktime')

# Merge using merge_asof
tolerance = 100  # Adjust this as needed
merged_df = pd.merge_asof(df_imu_data, df_afe_data, on='ticktime', tolerance=tolerance, direction='nearest')

# Save combined data to a JSON file
combined_data = merged_df.to_dict(orient='records')
with open('combined_data.json', 'w') as file:
    json.dump(combined_data, file, indent=4)

print("Data combined and saved to 'combined_data.json'")

import json
import pandas as pd
import matplotlib.pyplot as plt

import json
import pandas as pd

with open('combined_data.json', 'r') as file:
    combined_data = json.load(file)

df = pd.DataFrame(combined_data)

def extract_eye_movement(eye_data, eye_type):
    return [d['m'][0] for d in eye_data if d.get('t') == eye_type and 'm' in d and d['m']]

left_eye_movement = extract_eye_movement(df['afe_data'].explode(), 'L')
right_eye_movement = extract_eye_movement(df['afe_data'].explode(), 'R')

df_left_eye = pd.DataFrame(left_eye_movement)
df_right_eye = pd.DataFrame(right_eye_movement)

if not df_left_eye.empty:
    print("Left Eye Movement Descriptive Statistics:")
    print(df_left_eye.describe())

if not df_right_eye.empty:
    print("\nRight Eye Movement Descriptive Statistics:")
    print(df_right_eye.describe())

print("\nBasic analysis completed.")
